# README #

### What is this repository for? ###

* The new Arthaus website

### How do I get set up? ###

* Windows 10 machine
* Setup a local domain in hosts file (127.0.0.1 arthauswebsite.dev)
* Setup an IIS website using the new local domain
* Pull code down to machine to local repository folder
* Open VS as Admin
* Open Website (local repository folder)
* Setup publish profile, local files, same dir as IIS website

### Contribution guidelines ###

* Let me know if you are going to work on something as we may need to branch it

### Who do I talk to? ###

* JS / ML