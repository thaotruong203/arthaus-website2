﻿$(document).ready(function () {

    //Stickey sub navigation
    var mn = $(".sub-nav");
    mns = "sub-nav-scrolled";
    hdr = 140;

    $(window).scroll(function () {
        if ($(this).scrollTop() > hdr) {
            mn.addClass(mns);
        } else {
            mn.removeClass(mns);
        }
    });

    // Smooth scrolling
    var scrollLink = $('.scroll');
    var headerHeight = 135;

    scrollLink.click(function (e) {
        e.preventDefault();
        $('body,html').animate({
            scrollTop: $(this.hash).offset().top - headerHeight
        }, 1000)
    })

    // Active link switching
    $(window).scroll(function () {
        var scrollbarLocation = $(this).scrollTop();
        //console.log(scrollbarLocation)

        scrollLink.each(function () {

            var sectionOffset = $(this.hash).offset().top - 136;

            if (sectionOffset <= scrollbarLocation) {
                $(this).parent().addClass('active');
                $(this).parent().siblings().removeClass('active');
            }
        })
    })

    /* Animations */

    /* About page */
    $('#specialisms').css('opacity', 0);
    $('#specialisms').waypoint(function () {
        $('#specialisms').addClass('fadeIn');
    }, { offset: '80%' });

    $('#facts').css('opacity', 0);
    $('#facts').waypoint(function () {
        $('#facts').addClass('fadeIn');
        $('.count').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 1000,
                easing: 'linear',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                },
                complete: function () {
                    $this.text(this.countNum);
                    //alert('finished');
                }
            });
        });
    }, { offset: '80%' });

    $('#people').css('opacity', 0);
    $('#people').waypoint(function () {
        $('#people').addClass('fadeIn');
    }, { offset: '80%' });

    $('#banner').css('opacity', 0);
    $('#banner').waypoint(function () {
        $('#banner').addClass('fadeIn');
    }, { offset: '80%' });

    $('#clients').css('opacity', 0);
    $('.client-logo').css('opacity', 0);
    $('#clients').waypoint(function () {
        $('#clients').addClass('fadeIn');
        $('#client-logo1').addClass('fadeIn');
        $('#client-logo2').addClass('fadeIn');
        $('#client-logo3').addClass('fadeIn');
        $('#client-logo4').addClass('fadeIn');
        $('#client-logo5').addClass('fadeIn');
        $('#client-logo6').addClass('fadeIn');
        $('#client-logo7').addClass('fadeIn');
        $('#client-logo8').addClass('fadeIn');
        $('#client-logo9').addClass('fadeIn');
        $('#client-logo10').addClass('fadeIn');
        $('#client-logo11').addClass('fadeIn');
        $('#client-logo12').addClass('fadeIn');
        $('#client-logo13').addClass('fadeIn');
        $('#client-logo14').addClass('fadeIn');
        $('#client-logo15').addClass('fadeIn');
    }, { offset: '80%' });

    $('#testimonials').css('opacity', 0);
    $('#testimonials').waypoint(function () {
        $('#testimonials').addClass('fadeIn');
    }, { offset: '80%' });

    /* Expertise page */
    $('#branding-title').css('opacity', 0);
    $('#communications-title').css('opacity', 0);
    $('#digital-title').css('opacity', 0);

    $('#branding-title').waypoint(function () {
        $('#branding-title').addClass('fadeInLeft');
    }, { offset: '80%' });

    $('#communications-title').waypoint(function () {
        $('#communications-title').addClass('fadeInLeft');
    }, { offset: '80%' });

    $('#digital-title').waypoint(function () {
        $('#digital-title').addClass('fadeInLeft');
    }, { offset: '80%' });


    $('#branding-content').css('opacity', 0);
    $('#communications-content').css('opacity', 0);
    $('#digital-content').css('opacity', 0);

    $('#branding-content').waypoint(function () {
        $('#branding-content').addClass('fadeIn');
    }, { offset: '80%' });

    $('#communications-content').waypoint(function () {
        $('#communications-content').addClass('fadeIn');
    }, { offset: '80%' });

    $('#digital-content').waypoint(function () {
        $('#digital-content').addClass('fadeIn');
    }, { offset: '80%' });

    /* Contact page */
    $('#contact-left').css('opacity', 0);
    $('#contact-right').css('opacity', 0);

    $('#contact-left').waypoint(function () {
        $('#contact-left').addClass('fadeInLeft');
    }, { offset: '80%' });

    $('#contact-right').waypoint(function () {
        $('#contact-right').addClass('fadeInRight');
    }, { offset: '80%' });
})

