﻿var querySmall = "screen and (max-width: 759px)";
var queryMedium = "screen and (min-width: 760px) and (max-width: 1239px)";
var queryLarge = "screen and (min-width: 1240px)";

// or, for retina, less than 900 AND the retina media queries
//@media only screen and (-moz-min-device-pixel-ratio: 1.5),
//only screen and (-o-min-device-pixel-ratio: 3/2),
//only screen and (-webkit-min-device-pixel-ratio: 1.5),
//only screen and (min-device-pixel-ratio: 1.5) {
//    /*your rules*/
//}

var handleSmall = {
    match: function () {
        //console.log('moved into small');
        $('.blocksWrapper .middle').html(renderStructure("small"));
    },
    unmatch: function () {
        //console.log('moved out of small');
    }
};

var handleMedium = {
    match: function () {
        //console.log('moved into medium');
        $('.blocksWrapper .middle').html(renderStructure("medium"));
    },
    unmatch: function () {
        //console.log('moved out of medium');
    }
};

var handleLarge = {
    match: function () {
        //console.log('moved into large');
        $('.blocksWrapper .middle').html(renderStructure("large"));
    },
    unmatch: function () {
        //console.log('moved out of large');
    }
}

// run when loaded
$(document).ready(function () {


    //// grab an element
    //var myElement = document.querySelector(".top");
    //// construct an instance of Headroom, passing the element
    //var headroom = new Headroom(myElement);
    //// initialise
    //headroom.init();

    //$(".top").headroom();

    // run only on the home page
    // for the layout
    if ($('body').hasClass('home')) {
        // layout change
        $('.blocksContent').hide();
        $('.blocksWrapper .middle').hide();
        // reads the JSON file
        $.when(readJSON())
            .then(storeData, readFailed)
            .done(function () {
                enquire.register(querySmall, handleSmall);
                enquire.register(queryMedium, handleMedium);
                enquire.register(queryLarge, handleLarge);
            });
        $('.blocksWrapper .middle').show();
    }

    // run only on the work page
    // for the filter
    if ($('body').hasClass('work')) {

        // gets the projects already on the page
        var blocks = getProjects();
        storeProjectURLList(true);

        // output the sorted project types for the filter menu
        var typeList = getFilterOptions(); //generateFilterTypes(blocks);
        _.each(typeList, function (item) {
            var newItem = '<li>' + item + '</li>';
            $('.project-filter ul').append(newItem);
        });

        // define if prefiltered
        var hash = getHash();
        if (hash !== '') {

            console.log('show pre-filtered : ' + hash);

            outputFilteredProjects(blocks, hash)
            storeProjectURLList(false);

        }

        // make the filter clickable
        $('.blocksWrapper .middle .project-filter').on('click', 'li', function () {

            var $this = $(this);
            // don't do anything if the title is clicked
            if (!$this.hasClass('title')) {

                outputFilteredProjects(blocks, $this.text())
                storeProjectURLList(false);
            }

        });
    }

    if ($('body').hasClass('project')) {
        console.log('This is a project page');
        // initially hide the navigation
        //$('.projectNavigation').hide();
        // show prev / next based on the list of project URL's
        var projectList = getLocalData('projectURLs');
        if (projectList.length > 1) {
            var currentURL = getURLPath();
            var currentPosition = _.findIndex(projectList, { link: currentURL });
            // get the next project in the list
            var prevItem;
            var nextItem;

            if (!currentPosition > 0) {
                prevItem = projectList[projectList.length - 1];
            } else {
                prevItem = projectList[currentPosition - 1];
            }

            // it's at the end of the array
            if (currentPosition >= projectList.length - 1) {
                nextItem = projectList[0];
            } else {
                nextItem = projectList[currentPosition + 1];
            }

            $('.projectNavigation .prev').attr('title', prevItem.title);
            $('.projectNavigation .next').attr('title', nextItem.title);
            $('.projectNavigation').show();

            $('.projectNavigation').on('click', '.prev', function () {
                window.location.href = prevItem.link;
            });

            $('.projectNavigation').on('click', '.next', function () {
                window.location.href = nextItem.link;
            });
        }
    }

    // navigation
    $('.mobile-menu').click(function () {
        $('.nav-icon').toggleClass('open');
        //$('.menuItems ul').html('');
        //$('.desktop-menu').each(function () {
        //    $($(this)).clone().appendTo('.menuItems ul');
        //});
        $('.menuItems').toggleClass('open');
        $('.top').toggleClass('mobile-padding');
    });

    // footer date
    var d = new Date();
    var y = d.getFullYear();
    $('.cDate').text(y);

    // parallax effect

});

function outputFilteredProjects(projectsToFilter, filter) {

    // hide the projects
    $('.projects-list').hide();

    // get the projects which match the type clicked
    var projects = [];
    if (filter === 'all') {
        projects = projectsToFilter;
    } else {
        projects = getProjectsByType(projectsToFilter, filter);
    }

    // generate the filtered projects
    $('.filtered-projects').html(generateFilteredProjects(projects));

    // show the filtered projects
    $('.filtered-projects').show();


}

// gets a list of all the projects currently on the page
function getProjects() {

    var data = [];
    data = getLocalData("projects");
    if (!data.length > 0) {
        // store all the blocks into an array of html
        // with their properties
        var i = 0;
        var blocks = $('.projects-list .block').map(function () {
            var obj = {
                id: i++,
                html: $(this).html().trim(),
                classes: $(this).attr('class'),
                projectType: JSON.parse($(this).attr('data-projecttype'))
            };
            return obj;
        });
        data = blocks;
        storeLocalData(data, 'projects');
        //console.log('generating projects');
    }
    return data;
}

// reset and store a list of project URLs
// startingList (bool) = if the project list is the
// starting list of projects or a filtered one
function storeProjectURLList(startingList) {

    // don't run on project pages
    if ($('body').hasClass('project')) {
        return false;
    }
    console.log('Getting project URLs');
    clearLocalData('projectURLs');
    var rootElement = '.filtered-projects';
    if (startingList) {
        rootElement = '.projects-list';
    }
    var projectURLs = $(rootElement + ' .block .content').map(function () {
        var obj = {
            title: $(this).find('.blockFooter h2').text(),
            link: $(this).find('a').attr('href')
        };
        return obj;
    });
    storeLocalData(projectURLs, 'projectURLs');
    return true;
}

function getFilterOptions() {

    var projectData = getProjects()
    var data = [];
    data = getLocalData("filterOptions");
    if (!data.length > 0) {

        data = generateFilterTypes(projectData);
        storeLocalData(data, 'filterOptions');
        //console.log('generating filter options');
    }
    return data;
}

// filtered projects
function generateFilteredProjects(projects) {
    //console.log(projects);
    var numItems = projects.length;
    var stHTMLStruct = [];
    var mainRowCount = 1;
    var showMain = true;

    // how many times should a main row be shown
    // ~ https://www.joezimjs.com/javascript/great-mystery-of-the-tilde/
    // https://stackoverflow.com/questions/4228356/how-to-perform-integer-division-and-get-the-remainder-in-javascript
    var mainRowCount = ~~(numItems / 2);
    // how may remain
    var lastRowCols = numItems % 2;

    if (lastRowCols > 0) {
        mainRowCount += 1;
    }

    var counter = 0;
    // iterate over the number of main rows
    for (var mr = 1; mr <= mainRowCount; mr++) {

        stHTMLStruct.push(
            '<div class="blockMainRow">'
        );

        // iterate over the columns
        if (numItems > 1) {

            for (var i = 1; i <= 2; i++) {
                stHTMLStruct.push(outputBlock(projects[parseInt(counter)]));

                // increment the counter
                numItems -= 1;
                counter += 1;
            }
        } else {
            stHTMLStruct.push(outputBlock(projects[parseInt(counter)]));
        }

        stHTMLStruct.push(
            '</div>'
        );
    }

    return stHTMLStruct.join('');

}

function outputBlock(item) {

    //console.log(item);

    var outPutHTML = [];
    var blockClasses = item.classes;
    var blockHtml = item.html;
    outPutHTML.push(
        '<div class="blockCol">',
        '   <div class="blockRow">'
    );
    outPutHTML.push(
        '       <div class="' + blockClasses + '">'
    );
    outPutHTML.push(blockHtml);
    outPutHTML.push(
        '       </div>'
    );
    outPutHTML.push(
        '   </div>',
        '</div>'
    );

    return outPutHTML.join('');
}

function getProjectsByType(array, type) {

    var projects = [];

    _.each(array, function (project) {
        if (_.contains(project.projectType, type)) {
            projects.push(project);
        }
    });
    //var projects = _.where(array, { projectType: type });
    return projects;

}

function generateFilterTypes(array) {
    //console.log(array);

    // get all the types into an array
    var projectTypes = [];
    _.each(array, function (item) {
        if (item.projectType.length > 1) {
            _.each(item.projectType, function (pType) {
                projectTypes.push(pType);
            });
        } else {
            projectTypes.push(item.projectType[0]);
        }
    });

    // make them unique
    var types = _.uniq(projectTypes, function (pType) {
        return pType;
    });
    return _.sortBy(types, function (pType) {
        return pType;
    });
}

function storeData(data) {
    //console.log("data stored");
    storeLocalData(data, "AHStruct");
}

function readFailed() {
    console.log("read failed");
}

// return the JSON file if it succeeds in loading
function readJSON() {

    var d = $.Deferred();
    var locDat = getLocalData("AHStruct");
    if (locDat.length > 0) {
        //console.log('from session')
        d.resolve(locDat);
    } else {
        $.getJSON("/scripts/structure.min.json")
            .done(function (data) {
                console.log('from file ', data);
                d.resolve(data);
            })
            .fail(d.reject);
    }
    return d.promise();

}

// =============  Local storage functions  ================
function storeLocalData(array, storeName) {
    if (typeof (Storage) !== undefined) {
        clearLocalData(storeName);
        sessionStorage[storeName] = JSON.stringify(array);
        return true;
    } else {
        console.log("Could not store data to '" + storeName + "'");
        return false;
    }
}

function getLocalData(storeName) {
    var storeData = "";
    if (sessionStorage.hasOwnProperty(storeName)) {
        storeData = JSON.parse(sessionStorage[storeName]);
    }
    return storeData;
}

function clearLocalData(storeName) {
    if (sessionStorage.hasOwnProperty(storeName)) {
        sessionStorage.removeItem(storeName);
        return true;
    } else {
        console.log("No session store with the name '" + storeName + "'");
        return false;
    }
}

function renderStructure(size) {

    var requiredSizeData = getDataForSize(size);

    var STHTMLStruct = [];

    if (requiredSizeData.rowData.length > 0) {

        _.each(requiredSizeData.rowData, function (mainRow) {

            STHTMLStruct.push(
                '<div class="blockMainRow">'
            );

            _.each(mainRow, function (blockColumn) {


                _.each(blockColumn, function (blockRow) {

                    STHTMLStruct.push(
                        '<div class="blockCol">'
                    );

                    _.each(blockRow, function (blockData) {

                        _.each(blockData, function (blockItem) {

                            STHTMLStruct.push(
                                '<div class="blockRow">'
                            );

                            _.each(blockItem, function (subBlock) {

                                _.each(subBlock, function (item) {

                                    STHTMLStruct.push(
                                        outPutBlock(item)
                                    );

                                });

                            });
                            STHTMLStruct.push(
                                '</div>'
                            );

                        });
                    });
                    STHTMLStruct.push(
                        '</div>'
                    );
                });
            });
            STHTMLStruct.push(
                '</div>'
            );
        });
    }

    return STHTMLStruct.join('');

}

function getDataForSize(size) {

    var data = getLocalData("AHStruct");

    var requiredSizeData = _.find(data, function (d) {
        return d.type === size;
    });

    return requiredSizeData;
}

function outPutBlock(block) {
    var stHtml = [];

    //var blockType = ' type-' + block.blockType.toLowerCase();
    var blockNum = ' blockNum-' + block.blockNumber;
    var blockClasses = getBlockClasses(blockNum);
    var blockContent = getBlockContent(blockNum);
    stHtml.push(
        '<div class="',
        blockClasses,
        '">',
        blockContent,
        '</div>'
    );

    return stHtml.join('');

}

function getBlockContent(blockNum) {

    var blockContent = '';
    blockContent = $('.blocksContent .' + blockNum.trim()).html();

    return blockContent;
}

function getBlockClasses(blockNum) {

    var blockClasses = '';
    blockClasses = $('.blocksContent .' + blockNum.trim()).attr('class');

    return blockClasses;

}

// get's the hash from the current URL
function getHash() {
    var hash = window.location.hash;
    return hash.replace('#', '');
}

// get's the current URL path
function getURLPath() {
    var path = window.location.pathname;
    return path;
}

$('#contactSave').click(function () {
    console.log('Will it submit...');
    $('#contactUsModal form').submit();
});

$('#contact .modal-body form').on('submit', function (e) {
    e.preventDefault();
    var errorCounter = 0;

    var name = $('#recipient-name').val();
    var email = $('#recipient-email').val();
    var telephone = $('#recipient-telephone').val();
    var comments = $('#message-text').val();

    if (name === '') {
        errorCounter++;
    }

    if (email === '') {
        errorCounter++;
    }

    if (comments === '') {
        errorCounter++;
    }

    if (errorCounter === 0) {
        $.ajax({
            url: $(this).attr('action'),
            method: 'POST',
            data: {
                name: name,
                replyto: email,
                email: email,
                phone: telephone,
                message: comments,
                subject: 'A Message has been sent from the Arthaus Website',
            },
            dataType: "json",
            success: function () {
                $('#contactUsModal').modal('hide');
                alert('Your message has now been sent!');
            }

        });
    } else {
        alert('Not all required fields have been entered...');
    }
});

$('.footer-social-icon').hover(function () {
    var rolloverImage = '';

    if ($(this).hasClass('linkedin-icon')) {
        rolloverImage = '../images/footer/linkedin_rollover.png';
    }

    if ($(this).hasClass('twitter-icon')) {
        rolloverImage = '../images/footer/twitter_rollover.png';
    }

    if ($(this).hasClass('facebook-icon')) {
        rolloverImage = '../images/footer/facebook_rollover.png';
    }

    $(this).attr('src', rolloverImage);

}, function () {
    var nonRolloverImage = '';

    if ($(this).hasClass('linkedin-icon')) {
        nonRolloverImage = '../images/footer/LinkedIn.png';
    }

    if ($(this).hasClass('twitter-icon')) {
        nonRolloverImage = '../images/footer/Twitter.png';
    }

    if ($(this).hasClass('facebook-icon')) {
        nonRolloverImage = '../images/footer/Facebook.png';
    }

    $(this).attr('src', nonRolloverImage);
});


